<?php

require_once('vendor/autoload.php');
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\JavaScriptExecutor;

date_default_timezone_set('America/New_York');
$timestamp = date("ymd_h.i.s");
$max_timeout = 240 * 1000; // longer timeout is needed for mobile simulators and landscape orientations
$PROJECT = "NYU Giving Day";
$BUILD = $timestamp;
$USER = "lld";
$PASSWORD = "MA1n!pRin8~";
$START_URL = "http://" . $USER . ":" . $PASSWORD . "@test.nyuoneday.org";

// create folder for screenshots if non-existent
$screenshot_prefix = "screens/";
if(!is_dir($screenshot_prefix)) {
	print "Attempting to create screenshots directory.\n";
	if(!mkdir($screenshot_prefix)) {
		die('Failed to create screenshots folder');
	}
	print "Success\n";
}

// array of capabilities used. OS versions are left blank, which means server randomly chooses.
$view_land = array("deviceOrientation" => "landscape");
$view_port = array("deviceOrientation" => "portrait");

$windows_browsers = array("IE",  "IE",  "IE",   "IE",   "Edge", "Chrome",  "Firefox");
$windows_versions = array("8.0", "9.0",	"10.0", "11.0", "14.0", "55.0",    "50.0"   );

$osx_browsers = array("Chrome", "Firefox", "Safari", "Opera");
$osx_versions = array("55.0",   "50.0",    "10.0" /*no Opera version*/);

$mac_browsers = array("iPhone", "iPhone", "iPad");
$mac_devices = array("iPhone 6S Plus", "iPhone 6", "iPad Air 2");

$android_devices = array("Samsung Galaxy S5", "Google Nexus 5", "Google Nexus 4", "HTC One M8");

// construct caps list
$capslist = array();
$j = 0;

for($i = 0; $i < count($windows_browsers); $i++, $j++) {
	$capslist[$j] = array("browser" => $windows_browsers[$i], "browser_version" => $windows_versions[$i], "os" => "Windows");
}

for($i = 0; $i < count($osx_browsers); $i++, $j++) {
	$capslist[$j] = array("browser" => $osx_browsers[$i], "browser_version" => $osx_versions[$i], "os" => "OS X");
}

for($i = 0; $i < count($mac_devices); $i++, $j+=2) {
	$capslist[$j] = array("device" => $mac_devices[$i], "browserName" => $mac_browsers[$i], "platform" => "MAC");
	$capslist[$j+1] = $capslist[$j] + $view_port;
	$capslist[$j] += $view_land;
}

for($i = 0; $i < count($android_devices); $i++, $j+=2) {
	$capslist[$j] = array("device" => $android_devices[$i], "browserName" => "android", "platform" => "ANDROID");
	$capslist[$j+1] = $capslist[$j] + $view_land;
	$capslist[$j] += $view_port;
}

// real loop
foreach ($capslist as &$caps) {
	try {

		// use project and build name
		$caps["build"] = $BUILD;
		$caps["project"] = $PROJECT;

		// create filename prefix based on capabilities used
		if(array_key_exists("browser", $caps)) {
			$capsid = $caps["os"] . "_" . $caps["browser"] . "_" . $caps["browser_version"];
		} else {
			$capsid = $caps["platform"] . "_" . $caps["browserName"];
			if(array_key_exists("device", $caps)) {
				$capsid .= "_" . $caps["device"];
			}
			if(array_key_exists("deviceOrientation", $caps)) {
				$capsid .= "_" . $caps["deviceOrientation"];
			}
		}

		$web_driver = RemoteWebDriver::create("https://heatherlynnstryc1:jDcBjnwLaSXyk71EUGeg@hub-cloud.browserstack.com/wd/hub", $caps, $max_timeout, $max_timeout);

		$web_driver->get($START_URL);

		// display the currently running test
		print "running test: " . $capsid . "\n";

		sleep(3);

		// screenshot
		$web_driver->takeScreenshot($screenshot_prefix . "coming_soon_" . $capsid . "_1.png");

		$web_driver->executeScript("window.scrollTo(0,document.body.scrollHeight);");

		$web_driver->takeScreenshot($screenshot_prefix . "coming_soon_" . $capsid . "_2.png");

		$web_driver->quit();
	}
	catch(Exception $e) {
		// error handling: try to close the connection
		if(isset($web_driver)) {
			$web_driver->quit();
		}
		// print any errors that occur, but continue with testing
		echo 'ERROR: ' . $e->getMessage() . "\n";
	}
}

?>