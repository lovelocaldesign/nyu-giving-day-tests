# README #

PHP test suite for automating tests of NYU Giving Day project via BrowserStack.

How to run tests:

- `cd` into this directory
- install Composer
- run `php ./test`
